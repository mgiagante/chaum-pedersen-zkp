use chaum_pedersen_zkp::Proof;
use num_bigint::BigUint;
use std::env;
use std::io::stdin;
use zkp_auth::auth_client::AuthClient;
use zkp_auth::{AuthenticationAnswerRequest, AuthenticationChallengeRequest, RegisterRequest};

pub mod zkp_auth {
    include!("./zkp_auth.rs");
}

#[tokio::main]
async fn main() {
    let address = env::args()
        .nth(2)
        .expect("Address argument not found! Example: 127.0.0.1:50051");

    let connect_to = format!("https://{address}");

    println!("Connecting to {}", connect_to);

    let mut client = AuthClient::connect(connect_to.clone())
        .await
        .expect("Could not connect to server");
    println!("Connected to server");

    let mut buffer = String::new();

    // We ask for the username and password from the user through STDIN.
    println!("Username:");
    stdin()
        .read_line(&mut buffer)
        .expect("Could not input username");
    let username = buffer.trim().to_string();

    // Clear the buffer for the next read.
    buffer.clear();

    println!("Password:");
    stdin()
        .read_line(&mut buffer)
        .expect("Could not input password");
    let password = BigUint::from_bytes_be(buffer.trim().as_bytes());

    // Clear the buffer for the next read.
    buffer.clear();

    // We create and send the registration request.
    let (p, q, g, h) = Proof::get_constants();

    let y1 = Proof::exponentiate(&g, &password, &p);
    let y2 = Proof::exponentiate(&h, &password, &p);

    let register_request = RegisterRequest {
        user: username.clone(),
        y1: y1.to_bytes_be(),
        y2: y2.to_bytes_be(),
    };

    let _register_response = client
        .register(register_request)
        .await
        .expect("Error during registration");

    println!("Registered successfully as {}", username);

    // Then we create and send a challenge request in order to login.
    let k = chaum_pedersen_zkp::generate_random_number_below(&q);
    let r1 = Proof::exponentiate(&g, &k, &p);
    let r2 = Proof::exponentiate(&h, &k, &p);

    let challenge_request = AuthenticationChallengeRequest {
        user: username,
        r1: r1.to_bytes_be(),
        r2: r2.to_bytes_be(),
    };

    let challenge_response = client
        .create_authentication_challenge(challenge_request)
        .await
        .expect("Error during challenge");

    let response_data = challenge_response.into_inner();
    let auth_id = &response_data.auth_id;
    let challenge = BigUint::from_bytes_be(&response_data.c);

    println!("{:?}", response_data);

    println!("Password (to login):");
    stdin()
        .read_line(&mut buffer)
        .expect("Could not input password");
    let password = BigUint::from_bytes_be(buffer.trim().as_bytes());

    // Clear the buffer for the next read.
    buffer.clear();

    let proof = Proof::default();

    let solution = proof.solve(&k, &challenge, &password);

    let verification_request = AuthenticationAnswerRequest {
        auth_id: auth_id.clone(),
        s: solution.to_bytes_be(),
    };

    let verification_response = client
        .verify_authentication(verification_request)
        .await
        .expect("Error while verifying solution to challenge");

    println!(
        "You logged in! Session ID: {}",
        verification_response.into_inner().session_id
    );
}
