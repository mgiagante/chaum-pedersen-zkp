pub mod zkp_auth {
    include!("./zkp_auth.rs");
}

use chaum_pedersen_zkp::Proof;
use num_bigint::BigUint;
use std::collections::HashMap;
use std::sync::Mutex;

use tonic::{transport::Server, Code, Request, Response, Status};
use zkp_auth::{
    auth_server::{Auth, AuthServer},
    AuthenticationAnswerRequest, AuthenticationAnswerResponse, AuthenticationChallengeRequest,
    AuthenticationChallengeResponse, RegisterRequest, RegisterResponse,
};

#[derive(Debug, Default)]
struct ZKPAuthenticator {
    pub user_info: Mutex<HashMap<String, UserInfo>>,
    pub auth_id_to_user: Mutex<HashMap<String, String>>,
}

// user_name, y1 and y2 are for the registration.
// r1 and r2 are for the authorization.
// c, s and the session_id are for the verification.
#[derive(Debug, Default)]
struct UserInfo {
    pub user_name: String,
    pub y1: BigUint,
    pub y2: BigUint,
    pub r1: BigUint,
    pub r2: BigUint,
    pub c: BigUint,
}

#[tonic::async_trait]
impl Auth for ZKPAuthenticator {
    async fn register(
        &self,
        request: Request<RegisterRequest>,
    ) -> Result<Response<RegisterResponse>, Status> {
        println!("Processing user registration {:?}", request);

        let request = request.into_inner();

        let user_info = UserInfo {
            user_name: request.user,
            y1: BigUint::from_bytes_be(&request.y1),
            y2: BigUint::from_bytes_be(&request.y2),
            ..Default::default()
        };

        let stored_user_info = &mut self.user_info.lock().unwrap();
        stored_user_info.insert(user_info.user_name.to_owned(), user_info);

        Ok(Response::new(RegisterResponse {}))
    }

    async fn create_authentication_challenge(
        &self,
        request: Request<AuthenticationChallengeRequest>,
    ) -> Result<Response<AuthenticationChallengeResponse>, Status> {
        println!("Creating authentication challenge {:?}", request);

        let request = request.into_inner();

        let stored_user_info = &mut self.user_info.lock().unwrap();

        println!("{:?}", stored_user_info);

        if let Some(user_info) = stored_user_info.get_mut(&request.user) {
            let (_, q, _, _) = Proof::get_constants();
            let c = chaum_pedersen_zkp::generate_random_number_below(&q);
            let auth_id = chaum_pedersen_zkp::generate_random_string(12);

            let auth_id_to_user = &mut self.auth_id_to_user.lock().unwrap();

            auth_id_to_user.insert(auth_id.clone(), request.user);

            user_info.c = c.clone();
            user_info.r1 = BigUint::from_bytes_be(&request.r1);
            user_info.r2 = BigUint::from_bytes_be(&request.r2);

            Ok(Response::new(AuthenticationChallengeResponse {
                auth_id: auth_id.clone(),
                c: c.to_bytes_be(),
            }))
        } else {
            Err(Status::new(
                Code::NotFound,
                format!("User {} not found!", request.user),
            ))
        }
    }

    async fn verify_authentication(
        &self,
        request: Request<AuthenticationAnswerRequest>,
    ) -> Result<Response<AuthenticationAnswerResponse>, Status> {
        println!("Verifying Solution to Challenge {:?}", request);

        let request = request.into_inner();
        let auth_id = request.auth_id;

        let auth_id_to_user = &self.auth_id_to_user.lock().unwrap();

        if let Some(user_name) = auth_id_to_user.get(&auth_id) {
            let stored_user_info = &self.user_info.lock().unwrap();
            let user_info = &mut stored_user_info
                .get(user_name)
                .expect("User who requested this auth_id not found!");

            let s = BigUint::from_bytes_be(&request.s);

            let verification_passed = Proof::default().verify(
                &user_info.r1,
                &user_info.r2,
                &user_info.y1,
                &user_info.y2,
                &user_info.c,
                &s,
            );

            if verification_passed {
                let session_id = chaum_pedersen_zkp::generate_random_string(12);
                Ok(Response::new(AuthenticationAnswerResponse { session_id }))
            } else {
                Err(Status::new(
                    Code::Unauthenticated,
                    format!("AuthId: {} challenge not solved.", auth_id),
                ))
            }
        } else {
            Err(Status::new(
                Code::NotFound,
                format!("Auth ID {} not found!", auth_id),
            ))
        }
    }
}

#[tokio::main]
async fn main() {
    let port = std::env::args().nth(2).unwrap_or("50051".to_owned());
    let address = format!("0.0.0.0:{}", port)
        .parse::<std::net::SocketAddr>()
        .unwrap();

    println!("Server running on {}", address.to_owned());

    let zkp_authenticator = ZKPAuthenticator::default();

    Server::builder()
        .add_service(AuthServer::new(zkp_authenticator))
        .serve(address)
        .await
        .unwrap();
}
