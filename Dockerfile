FROM rust:1.70
WORKDIR /zkp-auth
COPY . .
RUN apt update && apt install -y protobuf-compiler
RUN cargo build --release --bin server
ENTRYPOINT cargo run --release --bin server
